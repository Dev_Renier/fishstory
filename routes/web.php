<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('admin/login', 'Auth\LoginController@login');
Route::post('admin/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->middleware(['web', 'auth'])->group(function () {
    Route::get('post', 'PostController@index')->name('post');
    Route::post('post/upload', 'PostController@store')->name('post.upload');
    Route::get('post/table', 'PostController@table')->name('post.table');
    Route::get('post/delete', 'PostController@destroy')->name('post.delete');

    Route::get('profile', 'ProfileController@index')->name('profile');
    Route::post('profile/update', 'ProfileController@update')->name('profile.update');
    Route::post('profile/show', 'ProfileController@show')->name('profile.show');
    Route::post('profile/changepass', 'ProfileController@changepass')->name('profile.changepass');

    Route::get('post/manager', 'PostManagerController@index')->name('post.manager');

    Route::get('profile/manager', 'ProfileManagerController@index')->name('profile.manager');
    Route::post('profile/manager/bind', 'ProfileManagerController@bind')->name('profile.manager.bind');
    Route::post('profile/manager/resetpass', 'ProfileManagerController@resetPass')->name('profile.manager.resetpass');
    Route::post('profile/manager/delete', 'ProfileManagerController@destroy')->name('profile.manager.delete');
    Route::post('profile/manager/update', 'ProfileManagerController@update')->name('profile.manager.update');
    Route::post('profile/manager/store', 'ProfileManagerController@store')->name('profile.manager.create');
});
