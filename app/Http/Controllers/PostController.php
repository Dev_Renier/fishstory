<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Post;
use FFMpeg;
use Yajra\DataTables\DataTables;
use DB;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public $dataTable;

    public function __construct(DataTables $dataTable)
    {
        $this->dataTable = $dataTable;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_page.posts');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        if ($request->file('video')) {
            $filename_video = str_replace(" ", "_", $request->file('video')->getClientOriginalName());
            $filename_thumbnail = str_replace(" ", "_", $request->file('image')->getClientOriginalName());

            $upload_video = $request->file('video')->storeAs('videos', $filename_video);
            $upload_thumbnail = $request->file('image')->storeAs('thumbnails', $filename_thumbnail);

            Post::query()->where('video', $upload_video)->delete();

            $type = $request->file('video')->getClientMimeType();

            $media = new Post;
            $media->user_id     = auth()->user()->id;
            $media->title       = $request->title;
            $media->tags        = $request->tags;
            $media->video       = $upload_video;
            $media->thumbnail   = $upload_thumbnail;
            $media->published   = 0;
            $media->save();
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $post = DB::table('posts')->where('id', $request->id)->first();

        Storage::delete($post->thumbnail);
        Storage::delete($post->video);
        DB::table('posts')->where('id', $request->id)->delete();

        return 'success';
    }

    public function table(Request $request)
    {
        $posts = DB::table('posts');

        return $this->dataTable->queryBuilder($posts)->make(true);
    }
}
