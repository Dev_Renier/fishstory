@extends('layouts.admin')

@section('content')
<div class="col-sm-6">
    <div class="card">
        <div class="card-header">
            <strong>Upload Form</strong>
            <small>User ID: {{ auth()->user()->id }}</small>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="POST" action="{{ route('post.upload') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Title</label>
                            <input class="form-control" name="title" type="text" placeholder="Enter Title">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Tags</label><small>(e.g. travel,art,fun)</small>
                            <input class="form-control" name="tags" type="text" placeholder="Enter Tags (e.g. travel,art,fun)">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Thumbnails</label>
                            <input class="form-control" id="file-input" type="file" name="image">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Video</label>
                            <input class="form-control" id="file-input" type="file" name="video">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary"> Upload Post </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-sm-10">
    <div class="card">
        <div class="card-header">
            <strong>Table Post</strong>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <table id="posts-table" class="display" style="width:100%"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
const app = new Vue({
    el: '#app',
    data: {
   
    },
    methods: {
        deleteVideo: function(id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this user video!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '{{ route("post.delete") }}',
                        method: 'GET',
                        data: {id : id},
                        success: function(data) {
                            swal("Poof! Video file has been deleted!", {
                                icon: "success",
                            }).then((refresh) => {
                                if(refresh) {
                                    window.location = '{{ route("post") }}';
                                }
                            })
                        }
                    });
                } else {
                    swal("User profile file is safe!");
                }
            });
        }
    },
    mounted: function() {
        var $this = this;
        var dt =$('#posts-table').DataTable({
            serverSide: true,
            processing: true,
            stateSave: true,
            ajax: "{{ route('post.table') }}",
            keys: true,
            "columns": [
                { "data": "id", 'name': 'id', 'title': 'ID' },
                { "data": "title", 'name': 'title', 'title': 'title' },
                { "data": "tags", 'name': 'tags', 'title': 'tags' },
                { "data": "published", 'name': 'published', 'title': 'published' },
                { 
                    "data": function(value) {
                        var video = '';
                        video += '<video id="my-video" class="video-js" controls preload="auto" width="230" height="100"';
                        video += 'poster="/storage/' + value.thumbnail + '" data-setup="{}">';
                        video += '<source src="/storage/' + value.video + '" type=\'video/mp4\'>';
                        video += '<source src="/storage/' + value.video + '" type=\'video/webm\'>';
                        video += '<p class="vjs-no-js">';
                        video += 'To view this video please enable JavaScript, and consider upgrading to a web browser that';
                        video += '<a href="/storage/' + value.video + '" target="_blank">supports HTML5 video</a>';
                        video += '</p>';
                        video += '</video>';
                        return video;
                    }, 
                    'name': 'video',
                    'title': 'Video' 
                },
                { 
                    "data": function(value) {
                        return '<button class="deleteVideo btn btn-sm btn-square btn-danger" type="button"><i class="fa fa-trash-o"></i> Delete</button>'
                    }, 
                    'name': 'id', 
                    'title': 'Actions' 
                },
            ],
            'drawCallback': function() {
                $(document).on('click', '.deleteVideo', function() {
                    id = dt.row($(this).parent().parent()).data().id;
                    $this.deleteVideo(id);
                });
            }
        });
    }
});
</script>
@endsection