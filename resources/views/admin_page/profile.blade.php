@extends('layouts.admin')

@section('content')

<div class="col-sm-6">
    <div class="card">
        <div class="card-header">
            <strong>My Profile</strong>
            <small>User ID: {{ auth()->user()->id }}</small>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" v-model="profile.name" type="text" placeholder="Enter your name">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>E-mail</label>
                        <input class="form-control" v-model="profile.email" type="e-mail" placeholder="Enter your E-mail">
                    </div>
                </div>
                <div class="col-sm-12">
                    <a href="#" class="btn btn-primary" v-on:click="updateData()"> Save Changes </a>
                    <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#profileModal"> Change Password </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-warning" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Change Password</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>New Password</label>
                        <input class="form-control" type="password" v-model="changepass.password" placeholder="Enter your New Password">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input class="form-control" type="password" v-model="changepass.password_confirmation" placeholder="Confirm New Password">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Old Password</label>
                        <input class="form-control" type="password" v-model="changepass.old_pass" placeholder="Enter your Old Password">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
            <button class="btn btn-warning" type="button" v-on:click="changePass()">Submit New Password</button>
        </div>
    </div>

    </div>

</div>
@endsection

@section('scripts')
<script>
const app = new Vue({
    el: '#app',
    data: {
        changepass: {
            id: '',
            password: '',
            old_pass: '',
            password_confirmation: ''
        },
        profile: {
            name: '',
            email: '',
            id: ''
        }
    },
    methods: {
        changePass: function() {
            var $this = this;
            $.ajax({
                url: '{{ route("profile.changepass") }}',
                method: 'POST',
                type: 'JSON',
                data: $this.changepass,
                success: function(data) {
                    if(data == 'success') {
                        $this.changepass.password = '';
                        $this.changepass.old_pass = '';
                        $this.changepass.password_confirmation = '';
                        swal("Good job!", "Password updated!", "success");
                    } else {
                        swal("Good job!", "Password does not match!", "error");
                    }
                },
                error: function(data) {
                    var jsonObj = JSON.parse(data.responseText);
                    var jsonPretty = JSON.stringify(jsonObj, null, '\t');
                    changepass.password = '';
                    changepass.old_pass = '';
                    changepass.password_confirmation = '';
                    swal({
                        title: "Error!",
                        text: jsonPretty,
                        icon: "error",
                        button: "Ok!",
                    });
                }
            });
        },
        updateData: function() {
            var $this = this;
            $.ajax({
                url: '{{ route("profile.update") }}',
                method: 'POST',
                type: 'JSON',
                data: $this.profile,
                success: function(data) {
                    swal("Good job!", "Profile updated!", "success");
                },
                error: function(data) {
                    var jsonObj = JSON.parse(data.responseText);
                    var jsonPretty = JSON.stringify(jsonObj, null, '\t');
                    swal({
                        title: "Error!",
                        text: jsonPretty,
                        icon: "error",
                        button: "Ok!",
                    });
                }
            });
        },
        getData: function() {
            var $this = this;
            $.ajax({
                url: '{{ route("profile.show") }}',
                method: 'POST',
                type: 'JSON',
                success: function(data) {
                    $this.profile.id = data.id;
                    $this.profile.email = data.email;
                    $this.profile.name = data.name;
                    $this.changepass.id = data.id;
                }
            });
        }
    },
    mounted: function() {
        this.getData();
    }
});
</script>
@endsection