@extends('layouts.admin')

@section('content')
<div class="col-sm-6">
    <div class="card">
        <div class="card-header">
            <strong>User Manager</strong>
            <small>Form</small>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#createModal">Create New User</button>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <table class="table table-responsive-sm table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>E-mail</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>
                                    {{ $user->id }}
                                </td>
                                <td>
                                    {{ $user->name }}
                                </td>
                                <td>
                                    {{ $user->role }}
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                                <td>
                                    {{ $user->created_at }}
                                </td>
                                <td>
                                    <button v-on:click="bind({{ $user->id }})" class="btn btn-warning btn-sm" type="button" data-toggle="modal" data-target="#editModal">Edit</button>
                                    <button v-on:click="deleteProfile({{ $user->id }})" class="btn btn-danger btn-sm">Delete</button>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-warning" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit User</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="hf-email">Email</label>
                            <div class="col-md-9">
                                <input class="form-control" type="email" v-model="profile.email" placeholder="Enter Email..">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Name</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" v-model="profile.name" placeholder="Enter Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Role</label>
                            <div class="col-md-9">
                                <select class="form-control" v-model="profile.role">
                                    <option value="0">Super Admin</option>
                                    <option value="1">Admin</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Password</label>
                            <div class="col-md-9">
                                <button v-on:click="resetPassword(profile.id)" class="btn btn-danger">Reset Password</button>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-warning" type="button" v-on:click="updateProfile">Save changes</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create User</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="hf-email">Email</label>
                        <div class="col-md-8">
                            <input class="form-control" type="email" v-model="profile.email" placeholder="Enter Email..">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Name</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" v-model="profile.name" placeholder="Enter Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Password</label>
                        <div class="col-md-8">
                            <input class="form-control" type="password" v-model="profile.password" placeholder="Enter Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">Confirm Password</label>
                        <div class="col-md-8">
                            <input class="form-control" type="password" v-model="profile.password_confirmation" placeholder="Enter Confirm Password">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="button" v-on:click="createUser">Save changes</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
const app = new Vue({
    el: '#app',
    data: {
        profile: {
            name: '',
            email: '',
            role: '',
            id: '',
            password: '',
            password_confirmation: ''
        }
    },
    methods: {
        bind(id) {
            var $this = this;
            $.ajax({
                url: '{{ route("profile.manager.bind") }}',
                method: 'POST',
                type: 'JSON',
                data: {id : id},
                success: function(data) {
                    $this.profile.name = data.name;
                    $this.profile.email = data.email;
                    $this.profile.role = data.role;
                    $this.profile.id = data.id;
                },
                error: function(data) {
                    var jsonObj = JSON.parse(data.responseText);
                    var jsonPretty = JSON.stringify(jsonObj, null, '\t');
                    swal({
                        title: "Error!",
                        text: jsonPretty,
                        icon: "error",
                        button: "Ok!",
                    })
                }
            });
        },
        createUser: function() {
            var $this = this;
            $.ajax({
                url: '{{ route("profile.manager.create") }}',
                method: 'POST',
                data: $this.profile,
                success: function(data) {
                    swal({
                        title: "Good Job!",
                        text: 'User has been created.',
                        icon: "success",
                        button: "Ok!",
                    }).then((refresh) => {
                        if(refresh) {

                            window.location = '{{ route("profile.manager") }}';
                        }
                    });
                },
                error: function(data) {
                    var jsonObj = JSON.parse(data.responseText);
                    var jsonPretty = JSON.stringify(jsonObj, null, '\t');
                    swal({
                        title: "Error!",
                        text: jsonPretty,
                        icon: "error",
                        button: "Ok!",
                    })
                }
            });
        },
        updateProfile: function() {
            var $this = this;
            $.ajax({
                url: '{{ route("profile.manager.update") }}',
                method: 'POST',
                data: $this.profile,
                success: function(data) {
                    swal({
                        title: "Good Job!",
                        text: 'User has been updated.',
                        icon: "success",
                        button: "Ok!",
                    }).then((refresh) => {
                        if(refresh) {

                            window.location = '{{ route("profile.manager") }}';
                        }
                    });
                }
            });
        },
        deleteProfile: function(id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this user profile!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '{{ route("profile.manager.delete") }}',
                        method: 'POST',
                        data: {id : id},
                        success: function(data) {
                            swal("Poof! User profile file has been deleted!", {
                                icon: "success",
                            }).then((refresh) => {
                                if(refresh) {

                                    window.location = '{{ route("profile.manager") }}';
                                }
                            })
                        }
                    });
                } else {
                    swal("User profile file is safe!");
                }
            });
        },
        resetPassword: function(id) {
            $.ajax({
                url: '{{ route("profile.manager.resetpass") }}',
                method: 'POST',
                data: {id : id},
                success: function(data) {
                    swal({
                        title: "Good Job!",
                        text: 'Password has been reset',
                        icon: "success",
                        button: "Ok!",
                    });
                }
            });
        }
    }
});
</script>
@endsection