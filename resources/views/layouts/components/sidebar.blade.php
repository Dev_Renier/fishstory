<div class="sidebar">
        <nav class="sidebar-nav">
          <ul class="nav">
            <li class="nav-title">Components</li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('post') }}">
                <i class="nav-icon icon-film"></i> My Posts
                <span class="badge badge-primary">{{ \App\Post::count() }}</span>
              </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('profile') }}">
                    <i class="nav-icon icon-user"></i> My Profile
                </a>
            </li>
            @if(auth()->user()->role == 0)
            <li class="nav-item">
                <a class="nav-link" href="{{ route('post.manager') }}">
                    <i class="nav-icon icon-direction"></i> Posts Manager
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('profile.manager') }}">
                    <i class="nav-icon icon-direction"></i> User Manager
                </a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="nav-icon icon-login icons"></i> Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
          </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
      </div>